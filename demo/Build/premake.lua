
project.name = "Juce_Demo_Plugin"
project.bindir = "."
project.libdir = project.bindir

project.configs = { "Debug", "Release" }

package = newpackage()
package.name = project.name
package.target = project.name
package.language = "c++"

package.objdir = project.bindir .. "/intermediate"
package.includepaths = { "/usr/include", "/usr/include/freetype2", "../../juce/source", "../JuceLibraryCode", "../Source" }
package.libpaths = { "/usr/X11R6/lib/", "../.." }
package.linkflags = { "static-runtime" }
package.kind = "dll"

package.config["Release"].target          = project.name
package.config["Release"].objdir          = package.objdir .. "/" .. project.name .. "_Release"
package.config["Release"].buildoptions    = { "-march=native -Os -fPIC -fvisibility=hidden" }
package.config["Release"].defines         = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JucePlugin_Build_LV2=1" }
package.config["Release"].links = { "freetype", "pthread", "rt", "X11", "GL", "juce" }

package.config["Debug"].target            = project.name
package.config["Debug"].objdir            = package.objdir .. "/" .. project.name .. "_Debug"
package.config["Debug"].buildoptions      = { "-march=native -ggdb -O0 -fPIC -fvisibility=hidden" }
package.config["Debug"].defines           = { "LINUX=1", "DEBUG=1", "_DEBUG=1", "JUCE_ALSA=0", "JucePlugin_Build_LV2=1" }
package.config["Debug"].links = { "freetype", "pthread", "rt", "X11", "GL", "juce_debug" }

package.files = {
  matchfiles (
    "../Source/*.cpp",
    "../JuceLibraryCode/*.cpp",
    "../../juce/source/src/audio/plugin_client/LV2/juce_LV2_Wrapper.cpp"
    )
}

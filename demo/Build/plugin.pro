# QtCreator project file

TARGET = Juce_Demo_Plugin

TEMPLATE = lib

QT -= core gui

CONFIG -= lex yacc uic resources qt link_prl no_mocdepend qt_no_framework
CONFIG += warn_off

DEFINES = LINUX JucePlugin_Build_LV2

CONFIG += release
#CONFIG += debug

DEFINES += _NDEBUG NDEBUG
#DEFINES += DEBUG _DEBUG

INCLUDEPATH = \
  ../Source \
  ../JuceLibraryCode \
  ../../juce/source \
  /usr/include/freetype2

LIBRARYPATH = ../..

SOURCES = \
  ../Source/PluginEditor.cpp \
  ../Source/PluginProcessor.cpp \
  ../../juce/source/src/audio/plugin_client/LV2/juce_LV2_Wrapper.cpp

HEADERS = \
  ../JuceLibraryCode/AppConfig.h \
  ../JuceLibraryCode/JuceHeader.h \
  ../JuceLibraryCode/JucePluginCharacteristics.h \
  ../Source/PluginEditor.h \
  ../Source/PluginProcessor.h

LIBS = -L../.. -lfreetype -lpthread -lrt -lX11 -lGL -ljuce

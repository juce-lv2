
project.name = "TAL-Reverb-2"
project.bindir = "."
project.libdir = project.bindir

project.configs = { "Debug", "Release" }

package = newpackage()
package.name = project.name
package.target = project.name
package.language = "c++"
package.linkflags = { "static-runtime" }
package.kind = "dll"

package.config["Release"].objdir = "intermediate/Release"
package.config["Debug"].objdir   = "intermediate/Debug"

package.config["Release"].target = "TAL-Reverb-2"
package.config["Debug"].target   = "TAL-Reverb-2_debug"

package.config["Release"].defines      = { "LINUX=1", "NDEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "JucePlugin_Build_LV2=1" };
package.config["Release"].buildoptions = { "-O2 -s -fvisibility=hidden -msse -ffast-math -static -fPIC" }
package.config["Release"].links        = { "freetype", "pthread", "rt", "X11", "GL", "juce" }

package.config["Debug"].defines        = { "LINUX=1", "DEBUG=1", "_DEBUG=1", "JUCE_ALSA=0", "JUCE_USE_VSTSDK_2_4=1", "JucePlugin_Build_LV2=1" };
package.config["Debug"].buildoptions   = { "-O0 -ggdb -static -fPIC" }
package.config["Debug"].links          = { "freetype", "pthread", "rt", "X11", "GL", "juce_debug" }

package.includepaths = {
    "../src",
    "../src/engine",
    "/usr/include",
    "/usr/include/freetype2",
    "../../juce/source",
    "." --fake
}

package.libpaths = {
    "/usr/X11R6/lib/",
    "/usr/lib/",
    "../../"
}

package.files = {
    matchfiles (
        "../src/*.cpp",
        "../../juce/source/src/audio/plugin_client/LV2/juce_LV2_Wrapper.cpp"
    )
}
